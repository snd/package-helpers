#!/bin/sh
#
#    Copyright (C) 2016-2017  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=5

. ./config

# Remove the arrow in the main menu button
sed 's/"tooltip", tooltip,/"tooltip", "Trisquel",/; s/"has-arrow", TRUE,/"has-arrow", FALSE,/' -i mate-panel/panel-menu-button.c

# Custom layout
cat << EOF > data/panel-default-layout.layout
[Toplevel bottom]
expand=true
orientation=bottom
size=42

[Object mate-menu]
locked=true
position=0
toplevel-id=bottom
object-type=menu


[Object files]
locked=true
position=20
launcher-location=/usr/share/applications/caja-home.desktop
toplevel-id=bottom
object-type=launcher
menu-path=applications:/

[Object browser]
locked=true
position=30
launcher-location=/usr/share/applications/browser.desktop
toplevel-id=bottom
object-type=launcher
menu-path=applications:/

[Object window-list]
object-type=applet
applet-iid=WnckletFactory::WindowListApplet
toplevel-id=bottom
position=40
locked=true

[Object notification-area]
object-type=applet
applet-iid=NotificationAreaAppletFactory::NotificationArea
toplevel-id=bottom
position=30
panel-right-stick=true
locked=true

[Object indicatorappletcomplete]
object-type=applet
applet-iid=IndicatorAppletCompleteFactory::IndicatorAppletComplete
toplevel-id=bottom
position=20
panel-right-stick=true
locked=true

[Object clock]
object-type=applet
applet-iid=ClockAppletFactory::ClockApplet
toplevel-id=bottom
position=10
panel-right-stick=true
locked=true

[Object showdesktopapplet]
locked=true
position=0
panel-right-stick=true
toplevel-id=bottom
applet-iid=WnckletFactory::ShowDesktopApplet
object-type=applet

EOF

cp data/panel-default-layout.layout data/ubuntu.layout
mv data/ubuntu.layout data/trisquel.layout
sed 's/ubuntu/trisquel/' -i data/Makefile.am

changelog "Changed default layout"

compile

